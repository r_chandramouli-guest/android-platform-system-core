NAME := libdiagnoseusb
SOURCES := diagnose_usb.cpp
SOURCES := $(foreach source, $(SOURCES), adb/$(source))
CPPFLAGS += -Iinclude -Ibase/include
LDFLAGS += -shared -Wl,-soname,$(NAME).so.0 \
	-Wl,-L/usr/lib/$(DEB_HOST_MULTIARCH)/android 

build: $(SOURCES)
	$(CC) $^ -o $(NAME).so.0  $(CPPFLAGS) $(LDFLAGS)
	ln -s $(NAME).so.0 $(NAME).so

clean:
	$(RM) $(NAME).so*

