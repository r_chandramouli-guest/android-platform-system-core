NAME = libadb


ADB_COMMON_CFLAGS := \
    -Wall -Wextra -Werror \
    -Wno-unused-parameter \
    -Wno-missing-field-initializers \
    -Wvla \
    -DADB_VERSION="\"$(tool_version)\"" \

ADB_COMMON_posix_CFLAGS := \
    -Wexit-time-destructors \
    -Wthread-safety \

ADB_COMMON_linux_CFLAGS := \
    $(ADB_COMMON_posix_CFLAGS) \

LIBADB_SRC_FILES := \
    adb.cpp \
    adb_io.cpp \
    adb_listeners.cpp \
    adb_trace.cpp \
    adb_utils.cpp \
    fdevent.cpp \
    sockets.cpp \
    socket_spec.cpp \
    sysdeps/errno.cpp \
    transport.cpp \
    transport_local.cpp \
    transport_usb.cpp \

LIBADB_CFLAGS := \
    $(ADB_COMMON_CFLAGS) \
     \

LIBADB_linux_CFLAGS := \
    $(ADB_COMMON_linux_CFLAGS) \

LIBADB_linux_SRC_FILES := \
    sysdeps_unix.cpp \
    sysdeps/posix/network.cpp \
    client/usb_dispatch.cpp \
    client/usb_libusb.cpp \
    client/usb_linux.cpp \

LOCAL_SRC_FILES := \
    $(LIBADB_SRC_FILES) \
    adb_auth_host.cpp \
    transport_mdns.cpp \
    $(LIBADB_linux_SRC_FILES)

SOURCES := $(foreach source, $(LOCAL_SRC_FILES), adb/$(source))
CXXFLAGS += -fpermissive -std=c++14 -fvisibility=hidden -fPIC 
CPPFLAGS += -Iadb -Iinclude -Ibase/include -Ilibcrypto_utils/include -Idebian/include \
            -DADB_HOST=1 -DADB_REVISION='"$(DEB_VERSION)"' -DADB_VERSION="\"$(tool_version)\""
LDFLAGS += -shared -Wl,-soname,$(NAME).so.0 \
           -Wl,-rpath=/usr/lib/$(DEB_HOST_MULTIARCH)/android \
           -lcrypto -lpthread -L. -lbase -lcutils -lcrypto_utils
#-ldnssd -ldiagnoseusb

build: $(SOURCES)
	$(CXX) $^ -o $(NAME).so.0 $(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS)
	ln -s $(NAME).so.0 $(NAME).so

clean:
	$(RM) $(NAME).so*
