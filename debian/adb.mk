NAME = adb

ADB_COMMON_CFLAGS := \
    -Wall -Wextra -Werror \
    -Wno-unused-parameter \
    -Wno-missing-field-initializers \
    -Wvla \
    -DADB_VERSION="\"$(tool_version)\"" \

ADB_COMMON_posix_CFLAGS := \
    -Wexit-time-destructors \
    -Wthread-safety \

ADB_COMMON_linux_CFLAGS := \
    $(ADB_COMMON_posix_CFLAGS) \

LOCAL_LDLIBS_linux := -lrt -ldl -lpthread

LOCAL_SRC_FILES := \
    adb_client.cpp \
    bugreport.cpp \
    client/main.cpp \
    console.cpp \
    commandline.cpp \
    file_sync_client.cpp \
    line_printer.cpp \
    services.cpp \
    shell_service_protocol.cpp \

CFLAGS += \
    $(ADB_COMMON_CFLAGS) \
    -D_GNU_SOURCE \
    -DADB_HOST=1 \
    $(ADB_COMMON_linux_CFLAGS) \

SOURCES := $(foreach source, $(SOURCES), adb/$(source))
CXXFLAGS += -std=c++14 -fpermissive
CPPFLAGS += -Iinclude -Iadb -Ibase/include \
            -DADB_REVISION='"$(DEB_VERSION)"'
LDFLAGS += -shared -Wl,-rpath=/usr/lib/$(DEB_HOST_MULTIARCH)/android -Wl,-rpath-link=. \
           -L/usr/lib/$(DEB_HOST_MULTIARCH)/android -lpthread -lssl -L. -ladb -lbase -lcutils -lcrypto_utils -ldiagnoseusb \

build: $(SOURCES)
	$(CXX) $^ -o adb/$(NAME) $(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS)

clean:
	$(RM) adb/$(NAME)

